package main

import (
	"AuthService/internal/config"
	"AuthService/internal/logging"
	"AuthService/internal/mongodb"
	"AuthService/internal/services"
	"AuthService/internal/web"
	"AuthService/internal/web/controllers"
	"go.uber.org/zap"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

func init() {
	rand.NewSource(time.Now().UnixNano())
}

func main() {
	cfg := config.InitConfiguration()
	logger := logging.NewLogger("auth.log")
	logger.Info("Connecting to MongoDB")
	mongoClient := mongodb.NewMongoDB(logger, &cfg.MongoDB)
	defer mongoClient.Release()
	err := mongoClient.Connect()
	if err != nil {
		logger.Fatal("MongoDB connection error", zap.Error(err))
	}
	logger.Info("Connected to MongoDB")

	wApp := web.NewWebServer(logger, &cfg.Web)
	registerRoutes(logger, wApp)
	go wApp.Run()

	//Graceful shutdown

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, os.Kill)
	takeSig := <-sigChan
	logger.Info("Shutting down gracefully", zap.String("signal", takeSig.String()))

}

func registerRoutes(logger *zap.Logger, wApp *web.WServer) {
	logger.Info("Registering routes")

	userServices := services.NewUserService(logger)

	wApp.RegisterRoutes([]controllers.Controller{
		controllers.NewAuthController(logger, userServices),
	})
}
