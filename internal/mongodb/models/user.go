package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type LoginType struct {
	ID   int    `bson:"id" json:"id"`
	Name string `bson:"name" json:"name"`
}

type User struct {
	ID          primitive.ObjectID `bson:"_id"`
	GUID        string             `bson:"guid" json:"guid"`
	Login       string             `bson:"login" json:"login"`
	LoginType   LoginType          `bson:"loginType" json:"loginType"`
	Name        string             `bson:"name_name" json:"last_name"`
	LastLoginAt time.Time          `bson:"last_login_at" json:"last_login_at"`
	CreatedAt   time.Time          `bson:"created_at" json:"created_at"`
}
