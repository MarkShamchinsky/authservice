package controllers

import (
	"github.com/gofiber/fiber/v2"
)

type Controller interface {
	GetGroup() string                 // return http path
	GetHandlers() []ControllerHandler // return handler
}

type ControllerHandler interface {
	GetMethod() string
	GetPath() string
	GetHandler() func(c *fiber.Ctx) error
}

type Handler struct {
	Method  string
	Path    string
	Handler func(c *fiber.Ctx) error
}

func (h *Handler) GetPath() string {
	return h.Path
}

func (h *Handler) GetHandler() func(c *fiber.Ctx) error {
	return h.Handler
}

func (h *Handler) GetMethod() string {
	return h.Method
}
