package controllers

import (
	"AuthService/internal/services"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type AuthController struct {
	log         *zap.Logger
	userService *services.UserService
}

func NewAuthController(log *zap.Logger, userService *services.UserService) *AuthController {
	return &AuthController{
		log:         log,
		userService: userService,
	}
}

func (c *AuthController) GetGroup() string {
	return "/auth"
}

func (c *AuthController) GetHandlers() []ControllerHandler {
	return []ControllerHandler{
		&Handler{
			Method:  "GET",
			Path:    "/auth",
			Handler: c.authHandler(),
		},
	}
}

func (c *AuthController) authHandler() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		return c.SendString("auth")
	}
}
