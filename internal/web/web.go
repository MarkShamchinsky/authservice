package web

import (
	"AuthService/internal/config"
	"AuthService/internal/web/controllers"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
	"reflect"
)

type WServer struct {
	log    *zap.Logger
	cfg    *config.WebServerConfig
	client *fiber.App
}

func NewWebServer(log *zap.Logger, cfg *config.WebServerConfig) *WServer {
	return &WServer{
		cfg:    cfg,
		log:    log,
		client: fiber.New(fiber.Config{}),
	}
}

func (w *WServer) RegisterRoutes(routes []controllers.Controller) {
	for _, route := range routes {
		group := w.client.Group(route.GetGroup())
		for _, handler := range route.GetHandlers() {
			switch handler.GetMethod() {
			case "GET":
				group.Get(handler.GetPath(), handler.GetHandler())
			case "POST":
				group.Post(handler.GetPath(), handler.GetHandler())
			case "PUT":
				group.Put(handler.GetPath(), handler.GetHandler())
			case "PATCH":
				group.Patch(handler.GetPath(), handler.GetHandler())
			case "DELETE":
				group.Delete(handler.GetPath(), handler.GetHandler())
			default:
				w.log.Error("Unsupported method",
					zap.String("controller", reflect.TypeOf(route).Elem().Name()),
					zap.String("path", handler.GetPath()),
					zap.String("method", handler.GetMethod()))
			}

		}

	}

}

func (w *WServer) Run() {
	w.log.Info("Starting web server")
	err := w.client.Listen(fmt.Sprintf("0.0.0.0:%s", w.cfg.Port))
	if err != nil {
		fmt.Println(fmt.Sprintf("Error starting web server, %s", err))
	}
}
