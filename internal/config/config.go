package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type WebServerConfig struct {
	Port string
}

type MongoDBConnectionConfig struct {
	Host     string
	Port     int
	Database string
	Username string
	Password string
}

type Config struct {
	MongoDB MongoDBConnectionConfig `mapstructure:"mongodb"`
	Web     WebServerConfig         `mapstructure:"web"`
}

// InitConfiguration initializes the configuration.
func InitConfiguration() *Config {
	var C *Config = new(Config)
	loadDefault()
	loadFile()
	viper.Unmarshal(C)
	return C
}

// loadDefault sets default values for MongoDB configuration settings.
func loadDefault() {
	viper.SetDefault("mongodb.host", "localhost")
	viper.SetDefault("mongodb.port", 27017)
	viper.SetDefault("mongodb.database", "mm-auth")
	viper.SetDefault("mongodb.username", "")
	viper.SetDefault("mongodb.password", "")

	viper.SetDefault("web.port", "8080")

}

// loadFile loads the configuration file in YAML format and sets the configuration settings.
func loadFile() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(fmt.Sprintf("Error reading config file, %s", err))
	}
}
